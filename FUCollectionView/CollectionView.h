//
//  CollectionView.h
//  FUCollectionView
//
//  Created by Eric Turner on 1/14/16.
//  Copyright © 2016 Eric Turner. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * This project demonstrates an inablility to drag a collectionView
 * cell into an empty section.
 *
 * In this example project there are two sections with separate
 * datasource arrays. Initially, the section 2 datasource array is empty
 * to demonstrate the problem.
 *
 * When section 1 datasource is empty, dragging from section 0 to
 * section 1 does not work.
 *
 * If there is at least 1 element in section 1, dragging from
 * section 0 to 1 can work.
 *
 * Note that when dragging a cell between sections, a
 * "repositioning event" must happen before the cell can be
 * dropped to the destination section.
 *
 * Use the "Reset" right bar button to alternately add and remove the
 * "11" element to/from the section 1 datasource array to change 
 * between empty and non-empty section 1 states.
 */
@interface CollectionView : UICollectionViewController

@end
