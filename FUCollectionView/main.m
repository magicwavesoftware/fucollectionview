//
//  main.m
//  FUCollectionView
//
//  Created by Eric Turner on 1/14/16.
//  Copyright © 2016 Eric Turner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
