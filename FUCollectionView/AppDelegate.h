//
//  AppDelegate.h
//  FUCollectionView
//
//  Created by Eric Turner on 1/14/16.
//  Copyright © 2016 Eric Turner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

