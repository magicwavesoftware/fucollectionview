//
//  FirstViewController.swift
//  Example
//
//  Created by Wojtek on 14/07/2015.
//  Copyright © 2015 NSHint. All rights reserved.
//

import UIKit

private let headerId = "MySectionHeader"

class FirstViewController: UICollectionViewController {

    var numbersTo10: [Int] = []
    var numbersOver10: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        for i in 0...100 {
//            numbers.append(i)
//        }
        for i in 0...10 {
            numbersTo10.append(i)
        }
//        for i in 11...20 {
        for i in 11..<12 {
            numbersOver10.append(i)
        }
        
        collectionView?.alwaysBounceVertical = true;
        self.configureFlowLayout(collectionView?.collectionViewLayout as! UICollectionViewFlowLayout)
    }
    
    func configureFlowLayout(layout: UICollectionViewFlowLayout){
        layout.headerReferenceSize = CGSize(width: view.frame.size.width, height: 32.0)
    }
    
    //ET 01/11/15
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        var headerView = UICollectionReusableView()
        
        if (kind == UICollectionElementKindSectionHeader) {
            headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: headerId, forIndexPath: indexPath)
        }
        
        return headerView
    }

    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 2
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (section == 0) ? numbersTo10.count : numbersOver10.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! TextCollectionViewCell
        let arrData = (indexPath.section == 0) ? numbersTo10 : numbersOver10;
        cell.textLabel.text = "\(arrData[indexPath.item])"
    
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, moveItemAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        
//        let temp = numbers.removeAtIndex(sourceIndexPath.item)
//        numbers.insert(temp, atIndex: destinationIndexPath.item)
        
        let fromSec = sourceIndexPath.section
        let toSec   = destinationIndexPath.section
        let fromIdx = sourceIndexPath.row
        let toIdx   = destinationIndexPath.row
        
        //swap
        if fromSec == toSec {
            if fromSec == 0 {
                swapArrItem(&numbersTo10, idx1: fromIdx, idx2: toIdx)
            }
            else if fromSec == 1 {
                swapArrItem(&numbersOver10, idx1: fromIdx, idx2: toIdx)
            }
        }
            //move
        else {
            if fromSec == 0 {
                moveItemBetweenArrs(&numbersTo10, idx1: fromIdx, arr2: &numbersOver10, idx2: toIdx)
            }
            else if fromSec == 1 {
                moveItemBetweenArrs(&numbersOver10, idx1: fromIdx, arr2: &numbersTo10, idx2: toIdx)
            }
        }
        
        //        collectionView.reloadData()
    }
    
    func swapArrItem(inout arr1: [Int], idx1: Int, idx2: Int){
        if idx1 == idx2 {
            return
        }
        let item = arr1[idx1]
        arr1[idx1] = arr1[idx2]
        arr1[idx2] = item
    }
    
    func moveItemBetweenArrs(inout arr1: [Int], idx1: Int, inout arr2: [Int], idx2: Int){
        let item = arr1[idx1]
        arr1.removeAtIndex(idx1)
        arr2.insert(item, atIndex: idx2)
    }
}

