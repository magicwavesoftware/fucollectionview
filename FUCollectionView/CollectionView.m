//
//  CollectionView.m
//  FUCollectionView
//
//  Created by Eric Turner on 1/14/16.
//  Copyright © 2016 Eric Turner. All rights reserved.
//

#import "CollectionView.h"


#pragma mark - FUSectionHeader
static NSString * const FUSectionHeader_ID = @"FUSectionHeader_ID";
static CGFloat const kHeaderHeight = 32.0; // IB config
@interface FUSectionHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *label;
@end
@implementation FUSectionHeader
@end

#pragma mark - FUCell
static NSString * const FUCell_ID = @"FUCell_ID";
@interface FUCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@end
@implementation FUCell
@end



@interface CollectionView ()
@property (strong, nonatomic) NSMutableArray *numbersTo10;
@property (strong, nonatomic) NSMutableArray *numbersOver10;
@end

@implementation CollectionView
{
    BOOL _resetWithSolution;
}


#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"LongPress to Reorder";
    
    /*
     * When section 1 datasource is empty, dragging from section 0 to
     * section 1 does not work. 
     *
     * If there is at least 1 element in section 1, dragging from 
     * section 0 to 1 can work.
     *
     * Note that when dragging a cell between sections, a 
     * "repositioning event" must happen before the cell can be
     * dropped to the destination section.
     *
     * Use the "Reset" right bar button to alternately add and remove the
     * "11" element to/from the section 1 datasource array to change
     * between empty and non-empty section 1 states.
     */
    [self initializeDatasourceArrays];
    
    // Set the state tracking ivar for first use.
    _resetWithSolution = YES;

    UICollectionView *cv = self.collectionView;
    self.collectionView.alwaysBounceVertical = YES;
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)cv.collectionViewLayout;
    layout.headerReferenceSize = CGSizeMake(cv.frame.size.width, kHeaderHeight);
    
    self.navigationItem.rightBarButtonItem = [self resetButton];
}


#pragma mark - UICollectionView/Datasource/Delegate Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (section == 0) ? _numbersTo10.count : _numbersOver10.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    FUSectionHeader *hView = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        hView = (FUSectionHeader *)[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:FUSectionHeader_ID forIndexPath:indexPath];
        
        hView.label.text = [NSString stringWithFormat:@"Section %d", (int)indexPath.section];
    }
    return hView;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FUCell *cell = (FUCell*)[collectionView dequeueReusableCellWithReuseIdentifier:FUCell_ID forIndexPath:indexPath];

    NSInteger idx = indexPath.row;
    cell.label.text = (indexPath.section == 0) ? _numbersTo10[idx] : _numbersOver10[idx];
    
    return cell;
}

#pragma mark - CollectionView Reordering

- (NSIndexPath *)collectionView:(UICollectionView *)collectionView targetIndexPathForMoveFromItemAtIndexPath:(NSIndexPath *)originalIndexPath toProposedIndexPath:(NSIndexPath *)proposedIndexPath
{
    NSLog(@"%s\n   originalIndexPath: %@\n     proposedIndexPath: %@",
          __PRETTY_FUNCTION__, originalIndexPath, proposedIndexPath);
    return proposedIndexPath;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath*)destinationIndexPath
{
    NSInteger fromSec = sourceIndexPath.section;
    NSInteger toSec   = destinationIndexPath.section;
    NSInteger fromIdx = sourceIndexPath.row;
    NSInteger toIdx   = destinationIndexPath.row;
    
    //swap
    if (fromSec == toSec) {
        NSMutableArray *arr = (fromSec == 0) ? _numbersTo10 : _numbersOver10;
        [self swapArrItem:arr idx1:fromIdx idx2:toIdx];
    }
    //move
    else {
        if (fromSec == 0) {
            [self moveItemBetweenArrs:_numbersTo10 idx1:fromIdx toArr:_numbersOver10 idx2:toIdx];
        }
        else if (fromSec == 1) {
            [self moveItemBetweenArrs:_numbersOver10 idx1:fromIdx toArr:_numbersTo10 idx2:toIdx];
        }
    }
}

- (void)swapArrItem:(NSMutableArray*)arr idx1:(NSInteger)idx1 idx2:(NSInteger)idx2 {
    if (idx1 == idx2) {
        return;
    }
    id item = arr[idx1];
    arr[idx1] = arr[idx2];
    arr[idx2] = item;
}

- (void)moveItemBetweenArrs:(NSMutableArray*)fromArr idx1:(NSInteger)idx1 toArr:(NSMutableArray*)toArr idx2:(NSInteger)idx2 {
    id item = fromArr[idx1];
    [fromArr removeObject:item];
    [toArr insertObject:item atIndex:idx2];
}


#pragma mark - Utilities

- (void)initializeDatasourceArrays {
    
    // section 0 datasource
    _numbersTo10   = [NSMutableArray array];
    
    // initialize section 0 with datasource elements
    for (int i=0; i <=10; i++) {
        [_numbersTo10 addObject:[NSString stringWithFormat:@"%d", i]];
    }

    // section 1 datasource
    _numbersOver10 = [NSMutableArray array];

    if (_resetWithSolution) {
        [_numbersOver10 addObject:@"11"];
    }
}

/**
 * Switch between empty and non-empty section 1 states.
 */
- (void)resetState {
    [self initializeDatasourceArrays];
    [self.collectionView reloadData];
    _resetWithSolution = !_resetWithSolution;
}

- (UIBarButtonItem *)resetButton {
    UIBarButtonItem *bbtn = [[UIBarButtonItem alloc] initWithTitle:@"Reset" style:UIBarButtonItemStylePlain target:self action:@selector(resetState)];
    return bbtn;
}

@end
